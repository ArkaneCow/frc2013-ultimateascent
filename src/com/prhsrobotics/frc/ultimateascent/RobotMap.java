package com.prhsrobotics.frc.ultimateascent;

import com.prhsrobotics.frc.ultimateascent.*;
import edu.wpi.first.wpilibj.*; 
/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
    // drive motors
    
    public static final int frontLeftDriveMotor = 1;
    public static final int rearLeftDriveMotor = 2;
    public static final int frontRightDriveMotor = 3;
    public static final int rearRightDriveMotor = 4;

    // shooter motors
    public static final int[] shooterMotor = {5, 6};
    public static final int feeder_motor = 2;
    public static final int elevation_motor = 1;
    
    // shooter limit switches
    public static final int shooter_high_limit_switch = 1;
    public static final int shooter_low_limit_switch = 2;
        
    // elevator potentiometer
    public static final int elevator_potentiometer = 2;
    
    //shooter state
    public static boolean left_bumper_state = false;
    public static boolean shooter_motors_state = false;
    
    // joysticks
    public static final int driverJoystick = 1;
    public static final int manipulatorJoystick = 2;
    
    // joystick sticks
    public static final int leftStickX = 1;
    public static final int leftStickY = 2;
    public static final int triggers = 3;
    public static final int rightStickX = 4;
    public static final int rightStickY = 5;
    public static final int DPadLeftRight = 6;
    
    //joystick buttons
    public static final int button_a = 1; //button A
    public static final int button_b= 2; //button B
    public static final int button_c = 3; //button X
    public static final int button_d = 4; //button Y
    public static final int left_bumper = 5;
    public static final int right_bumper = 6;
    
}
