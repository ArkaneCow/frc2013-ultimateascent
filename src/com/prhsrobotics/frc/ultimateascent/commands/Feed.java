/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc.ultimateascent.commands;

import edu.wpi.first.wpilibj.Relay;

/**
 *
 * @author pillybook
 */
public class Feed extends CommandBase {
    
    public Feed() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(shooter_relays);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        shooter_relays.feeder_spin(Relay.Value.kForward);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
        shooter_relays.feeder_stop();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        shooter_relays.feeder_stop();
    }
}