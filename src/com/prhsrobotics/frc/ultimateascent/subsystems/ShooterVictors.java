package com.prhsrobotics.frc.ultimateascent.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.*;
import com.prhsrobotics.frc.ultimateascent.RobotMap;
import edu.wpi.first.wpilibj.Relay.Value;
import edu.wpi.first.wpilibj.camera.AxisCamera;
import edu.wpi.first.wpilibj.image.HSLImage;
/**
 * Controls for the chassis subsystem of the robot.
 * @author Nitant
 */
public class ShooterVictors extends Subsystem {
    
    /**
     * Specifies no default command for the system.
     */
    Victor shooter_left = new Victor(1, RobotMap.shooterMotor[0]);
    Victor shooter_right = new Victor(1, RobotMap.shooterMotor[1]);
    
    public void shooter_motors(double left, double right)
    {
        shooter_left.set(left);
        shooter_right.set(right);
    }
    public void shooter_spin(double speed)
    {
        shooter_motors(speed, speed); 
    }
    public void shooter_stop()
    {
        shooter_left.set(0);
        shooter_right.set(0);
    }
    protected void initDefaultCommand() {
        
    }
}