package com.prhsrobotics.frc.ultimateascent.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.*;
import com.prhsrobotics.frc.ultimateascent.RobotMap;
import edu.wpi.first.wpilibj.Relay.Value;
/**
 * Controls for the chassis subsystem of the robot.
 * @author Nitant
 */
public class ShooterRelays extends Subsystem {
    
    /**
     * Specifies no default command for the system.
     */
    Relay feeder = new Relay(1, RobotMap.feeder_motor);
    Relay elevation = new Relay(1, RobotMap.elevation_motor);
    
    public void elevator_spin(Value value)
    {
        elevation.set(value); 
    }
    public void elevator_stop()
    {
        elevation.set(Relay.Value.kOff); 
    }
    public void feeder_spin(Value value)
    {
        feeder.set(value); 
    }
    public void feeder_stop()
    {
        feeder.set(Relay.Value.kOff);
    }
    protected void initDefaultCommand() {
        
    }
}