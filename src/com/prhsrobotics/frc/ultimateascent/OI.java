
package com.prhsrobotics.frc.ultimateascent;

import com.prhsrobotics.frc.ultimateascent.*;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.DigitalIOButton;
import com.prhsrobotics.frc.ultimateascent.RobotMap; 
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import com.prhsrobotics.frc.ultimateascent.commands.*; 
/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
    //controllers
    public static Joystick driverJoystick, manipulatorJoystick; 
    static Button spin_button, shoot_button, elevator_up, elevator_down;
    //used to initialize
    public OI()
    {
        init();
    }
    public static void init()
    {
        driverJoystick = new Joystick(RobotMap.driverJoystick);
        manipulatorJoystick = new Joystick(RobotMap.manipulatorJoystick);
        spin_button = new JoystickButton(manipulatorJoystick, RobotMap.left_bumper);
        shoot_button = new JoystickButton(manipulatorJoystick, RobotMap.right_bumper);
        elevator_up = new JoystickButton(manipulatorJoystick, RobotMap.button_c);
        elevator_down = new JoystickButton(manipulatorJoystick, RobotMap.button_d);
        spin_button.whenPressed(new Spin());
        shoot_button.whileHeld(new Feed());
        elevator_up.whileHeld(new ElevatorUp());
        elevator_down.whileHeld(new ElevatorDown()); 
    }
    //// CREATING BUTTONS
    // One type of button is a joystick button which is any button on a joystick.
    // You create one by telling it which joystick it's on and which button
    // number it is.
    // Joystick stick = new Joystick(port);
    // Button button = new JoystickButton(stick, buttonNumber);
     
    // Another type of button you can create is a DigitalIOButton, which is
    // a button or switch hooked up to the cypress module. These are useful if
    // you want to build a customized operator interface.
    // Button button = new DigitalIOButton(1);
    
    // There are a few additional built in buttons you can use. Additionally,
    // by subclassing Button you can create custom triggers and bind those to
    // commands the same as any other Button.
    
    //// TRIGGERING COMMANDS WITH BUTTONS
    // Once you have a button, it's trivial to bind it to a button in one of
    // three ways:
    
    // Start the command when the button is pressed and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whileHeld(new ExampleCommand());
    
    // Run the command while the button is being held down and interrupt it once
    // the button is released.
    // button.whileHeld(new ExampleCommand());
    
    // Start the command when the button is released  and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenReleased(new ExampleCommand());
    
}


