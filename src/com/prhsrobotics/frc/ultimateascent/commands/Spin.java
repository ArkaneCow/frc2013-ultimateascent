/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc.ultimateascent.commands;

import com.prhsrobotics.frc.ultimateascent.OI;
import com.prhsrobotics.frc.ultimateascent.RobotMap;

/**
 *
 * @author pillybook
 */
public class Spin extends CommandBase {
    
    public Spin() {
        // Use requires() here to declare subsystem dependencies
    // Called just before this Command runs the first time
        // eg. requires(chassis);
        requires(shooter_victors);

    }
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        if(OI.manipulatorJoystick.getRawButton(RobotMap.left_bumper) && !RobotMap.left_bumper_state){
            RobotMap.left_bumper_state = true;
            if(!RobotMap.shooter_motors_state){
                shooter_victors.shooter_spin(1.0);
                RobotMap.shooter_motors_state=true;
            }
            else if (RobotMap.shooter_motors_state){
                shooter_victors.shooter_spin(0);
                RobotMap.shooter_motors_state=false;
            }
        }
        else if(!OI.manipulatorJoystick.getRawButton(RobotMap.left_bumper) && RobotMap.left_bumper_state){
            RobotMap.left_bumper_state=false;
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
        shooter_victors.shooter_stop();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        shooter_victors.shooter_stop();
    }
}