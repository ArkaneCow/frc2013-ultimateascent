/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc.ultimateascent.subsystems;

import com.prhsrobotics.frc.ultimateascent.RobotMap;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author RoboLions
 */
public class Sensors extends Subsystem{

    DigitalInput elevator_lower_limit = new DigitalInput(1,RobotMap.shooter_low_limit_switch);
    DigitalInput elevator_upper_limit = new DigitalInput(1,RobotMap.shooter_high_limit_switch);
    
    public boolean lower_switch_value(){
        return elevator_lower_limit.get();
    }
    
    public boolean upper_switch_value(){
        return elevator_upper_limit.get();
    }
        
    protected void initDefaultCommand(){
        
    }
}
