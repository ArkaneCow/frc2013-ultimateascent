/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc.ultimateascent.commands;

import com.prhsrobotics.frc.ultimateascent.OI;
import com.prhsrobotics.frc.ultimateascent.RobotMap;

/**
 *
 * @author pillybook
 */
public class SpinAutonomous extends CommandBase {
    
    public SpinAutonomous() {
        // Use requires() here to declare subsystem dependencies
    // Called just before this Command runs the first time
        // eg. requires(chassis);
        requires(shooter_victors);

    }
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        shooter_victors.shooter_spin(1);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
        shooter_victors.shooter_stop();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        shooter_victors.shooter_stop();
    }
}