/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc.ultimateascent.subsystems;

import com.prhsrobotics.frc.ultimateascent.RobotMap;
import edu.wpi.first.wpilibj.AnalogChannel;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author RoboLions
 */
public class PIDSensors extends Subsystem{
    
    AnalogChannel elevator_potentiometer = new AnalogChannel(1 , RobotMap.elevator_potentiometer);
    
    public double getAngle(){
        return elevator_potentiometer.pidGet();
    }
    
    public void initDefaultCommand(){
        
    }
    
}
