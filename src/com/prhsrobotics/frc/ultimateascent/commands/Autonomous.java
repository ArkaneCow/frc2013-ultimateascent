/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc.ultimateascent.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;

/**
 *
 * @author Nitant
 */
public class Autonomous extends CommandGroup {
    
    public Autonomous(){
        addParallel(new SpinAutonomous());
        addSequential(new Wait() , 2);
        for(int x = 0 ; x<5 ; x++){
            addSequential(new Feed() , .425);
            addSequential(new Wait() , 1);
        }
        // Add Commands here:
        // e.g. addSequential(new Command1());
        //      addSequential(new Command2());
        // these will run in order.

        // To run multiple commands at the same time,
        // use addParallel()
        // e.g. addParallel(new Command1());
        //      addSequential(new Command2());
        // Command1 and Command2 will run in parallel.

        // A command group will require all of the subsystems that each member
        // would require.
        // e.g. if Command1 requires chassis, and Command2 requires arm,
        // a CommandGroup containing them would require both the chassis and the
        // arm.
    }
}