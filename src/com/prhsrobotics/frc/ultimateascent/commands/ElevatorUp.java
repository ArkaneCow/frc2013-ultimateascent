/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc.ultimateascent.commands;

import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author pillybook
 */
public class ElevatorUp extends CommandBase {
    
    public ElevatorUp() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(shooter_relays);
        requires(pid_sensors);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        if(sensors.upper_switch_value()){
            shooter_relays.elevator_spin(Relay.Value.kForward);
        }
        else if(!sensors.upper_switch_value()){
            shooter_relays.elevator_stop();
        }
        SmartDashboard.putNumber("Elevator Angle", pid_sensors.getAngle());
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
        shooter_relays.elevator_stop();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        shooter_relays.elevator_stop();
    }
}